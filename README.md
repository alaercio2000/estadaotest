## Teste desenvolvedor Estadão

## Índice

[TOC]

## Requisitos
- PHP 7.4 ou superior
- Composer
- MySQL

## Instalação

#### Clonar projeto
    git clone https://Alaercio2000@bitbucket.org/alaercio2000/estadaotest.git

#### Instalar dependências
Dentro da pasta do projeto execute o comando:

    composer install

#### Criar Schema
É necessário que seja criado uma nova shema na banco de dados, exemplo:

    CREATE SCHEMA `name_database` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;

#### Configurar variáveis de ambiente
É necessário que seja criado um arquivo com o nome ‘.env’ na raiz do projeto, use o arquivo ‘.env.example’ como base para isso.
Após criado, será necessário editar as as variáveis abaixo com as credenciais do seu banco de dados.

    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=

#### Comandos para o projeto funcionar
Será necessário rodar os seguintes comandos respectivamente para funcionamento do projeto, o ultimo comando poderá demorar dependendo do hardware do computador.

    php artisan key:generate
    php artisan migrate
    php artisan db:seed

#### Comando para startar o projeto
    php artisan serve
