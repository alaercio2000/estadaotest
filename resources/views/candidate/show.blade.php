@extends('layouts.template')

@section('title')
    {{ $candidate->name }}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/candidate/show.css') }}">
@endsection

@section('content')
    <div class="content">
        <header>
            <a class="button-back" href="{{ route('candidate.index') }}">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="30px" widht="30px" fill="#000000">
                    <path
                        d="M447.1 256C447.1 273.7 433.7 288 416 288H109.3l105.4 105.4c12.5 12.5 12.5 32.75 0 45.25C208.4 444.9 200.2 448 192 448s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L109.3 224H416C433.7 224 447.1 238.3 447.1 256z" />
                </svg>
            </a>
            <h2 class="title ml-5">
                {{ $candidate->name }}
            </h2>

            <div class="list-actions">

                <a href="{{ route('candidate.edit', ['candidate' => $candidate->id]) }}" class="btn btn-warning"
                    title="Editar">
                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FFFFFF">
                        <path d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z" />
                    </svg>
                </a>

                <form action="{{ route('candidate.destroy', ['candidate' => $candidate->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger" title="Excluir">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px"
                            fill="#FFFFFF">
                            <path d="M0 0h24v24H0z" fill="none" />
                            <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
                        </svg>
                    </button>
                </form>
            </div>
        </header>

        @if (session('updated'))
            <div data-dismiss="alert" class="alert alert-success mt-4" role="alert">
                {{ session('updated') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="d-flex flex-column mt-4">
            <span><b>E-mail: </b>{{ $candidate->email }}</span>
            <span><b>Criado em: </b>{{ date_format($candidate->created_at, 'd/m/y G:i') }}</span>
        </div>

        <section>
            <div class="d-flex justify-content-between mt-4">
                <h3>Vagas candidatadas</h3>

                @if ($vacancies->all())
                    <button type="submit" class="btn btn-danger" data-target="#removeCandidatesModal" data-toggle="modal">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px"
                            fill="#FFFFFF">
                            <path d="M0 0h24v24H0z" fill="none" />
                            <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
                        </svg>
                    </button>
                @endif
            </div>

            @if (session('vaga removed'))
                <div data-dismiss="alert" class="alert alert-danger mt-4" role="alert">
                    {{ session('vaga removed') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if ($vacancies->all())
                <div class="row">
                    @foreach ($vacancies as $vacancy)
                        <div class="col-md-6 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <a class="link-title"
                                            href="{{ route('vacancy.show', ['vacancy' => $vacancy->vacancy_id]) }}">
                                            <h5 class="card-title">
                                                {{ $vacancy->title }}
                                            </h5>
                                        </a>
                                        <h6>{{ date_format($vacancy->created_at, 'd M') }}</h6>
                                    </div>

                                    <div class="d-flex justify-content-between">
                                        <p class="card-text p-1">
                                            {{ strlen($vacancy->description) > 250 ? substr($vacancy->description, 0, 245) . '...' : $vacancy->description }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <h6 class="ml-3 mt-3">Não há vagas</h6>
            @endif
        </section>

        <form method="POST" action="{{ route('vacancy.remove.candidates') }}">
            @csrf
            @method('DELETE')
            <div class="modal fade" id="removeCandidatesModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    @csrf
                    <div class="modal-content p-0">
                        <div class="modal-header">
                            <h5 class="modal-title">Removendo vagas candidatadas</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5>Vags</h5>
                            @foreach ($vacancies as $vacancy)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="vacancies_candidates[]"
                                        value="{{ $vacancy->id }}" id="check-remove{{ $vacancy->id }}">
                                    <label class="form-check-label" for="check-remove{{ $vacancy->id }}">
                                        {{ $vacancy->title }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-danger">Remover candidaturas</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
