@extends('layouts.template')

@section('title')
    @if (empty($candidate))
        Adicionando novo candidato
    @else
        Atualizando candidato
    @endif
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('assets/css/candidate/action.css')}}">
@endsection

@section('content')
    <div class="content">
        <a class="button-back mt-n2" href="@if(empty($candidate)) {{ route('candidate.index') }} @else {{ route('candidate.show', ['candidate' => $candidate->id]) }} @endif">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="30px" widht="30px" fill="#000000">
                <path
                    d="M447.1 256C447.1 273.7 433.7 288 416 288H109.3l105.4 105.4c12.5 12.5 12.5 32.75 0 45.25C208.4 444.9 200.2 448 192 448s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L109.3 224H416C433.7 224 447.1 238.3 447.1 256z" />
            </svg>
        </a>
        <h2 class="title">
            @if (empty($candidate))
                Adicionando novo candidato
            @else
                Atualizando candidato
            @endif
        </h2>

        <form action="@if(empty($candidate)) {{route('candidate.store')}} @else {{route('candidate.update', ['candidate' => $candidate->id])}} @endif" method="POST">
            @csrf
            @if (!empty($candidate))
                @method('PUT')
            @endif

            <div class="form-group">
                <label for="name">Nome do candidato</label>
                <input class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Digite o nome do candidato..." value="@if(empty($candidate)){{old('name')}}@else{{$candidate->name}}@endif" />
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('name') }}
                </div>
            </div>

            <div class="form-group">
                <label for="email">E-mail do candidato</label>
                <input class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Digite o e-mail do candidato..." value="@if(empty($candidate)){{old('email')}}@else{{$candidate->email}}@endif" />
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('email') }}
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">
                    @if(empty($candidate)) Adicionar @else Atualizar @endif
                </button>
            </div>
        </form>
    </div>
@endsection
