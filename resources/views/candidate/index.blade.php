@extends('layouts.template')

@section('title', 'Candidatos')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/candidate/index.css') }}">
@endsection

@section('content')

    <div class="content">
        <div>
            <a class="button-back" href="{{ route('home') }}">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="30px" widht="30px" fill="#000000">
                    <path
                        d="M447.1 256C447.1 273.7 433.7 288 416 288H109.3l105.4 105.4c12.5 12.5 12.5 32.75 0 45.25C208.4 444.9 200.2 448 192 448s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L109.3 224H416C433.7 224 447.1 238.3 447.1 256z" />
                </svg>
            </a>
            <h1 class="title-list mb-5">Lista de Candidatos</h1>
        </div>

        @if (session('created'))
            <div data-dismiss="alert" class="alert alert-success" role="alert">
                {{ session('created') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('destroyed'))
            <div data-dismiss="alert" class="alert alert-danger" role="alert">
                {{ session('destroyed') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <main>
            <div class="d-flex justify-content-between">
                <a href="{{ route('candidate.create') }}" class="btn btn-success">
                    Adicionar novo candidato
                </a>

                <div class="d-flex">

                    <form action="{{ route('candidate.index') }}" class="form-inline" method="GET">
                        <div class="form-group">
                            <input type="hidden" value={{$candidates->perPage()}} name="per_page">
                            <label for="list_order">Ordenar por</label>
                            <select id="list_order" class="form-control ml-3 mr-4" name="order" onchange="this.form.submit()">
                                <option @if ($order == 1) selected @endif value="1">Recentemente criado
                                </option>
                                <option @if ($order == 2) selected @endif value="2">Mais antigos</option>
                                <option @if ($order == 3) selected @endif value="3">Atualizados recentemente
                                </option>
                                <option @if ($order == 4) selected @endif value="4">Mais desatualizado
                                </option>
                            </select>
                        </div>
                    </form>

                    <form action="{{ route('candidate.index') }}" class="form-inline" method="GET">
                        <div class="form-group">
                            <input type="hidden" value={{$order}} name="order">
                            <label for="list_order">resultados por página</label>
                            <select id="list_order" class="form-control ml-3" name="per_page" onchange="this.form.submit()">
                                <option @if($candidates->perPage() == 20) selected @endif >20</option>
                                <option @if($candidates->perPage() == 50) selected @endif >50</option>
                                <option @if($candidates->perPage() == 100) selected @endif >100</option>
                                <option @if($candidates->perPage() == 1000) selected @endif >1000</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>

            @foreach ($candidates as $candidate)
                <div class="mt-3">
                    <div class="card p-3">
                        <div class="d-flex justify-content-between">
                            <h5 class="card-title">
                                {{ $candidate->name }}
                            </h5>
                            <h6>{{ date_format($candidate->created_at, 'd M') }}</h6>
                        </div>

                        <div class="d-flex justify-content-between">
                            <p class="card-text">
                                {{ $candidate->email }}
                            </p>

                            <a href="{{ route('candidate.show', ['candidate' => $candidate->id]) }}"
                                class="btn btn-primary">
                                Ver mais
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="mt-4">
                {{ $candidates->withQueryString()->links() }}
            </div>
        </main>
    </div>

@endsection
