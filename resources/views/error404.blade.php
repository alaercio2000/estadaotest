@extends('layouts.template')

@section('title', 'Página não encontrada')

@section('content')
    <h1 class="text-center mt-4">
        Página não encontrada
    </h1>
@endsection
