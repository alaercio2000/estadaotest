@extends('layouts.template')

@section('title', 'Vagas')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/vacancy/index.css') }}">
@endsection

@section('js')
    <script src="{{ asset('assets/js/vacancy/index.js') }}"></script>
@endsection

@section('content')
    <div class="content">
        <div>
            <a class="button-back" href="{{ route('home') }}">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="30px" widht="30px" fill="#000000">
                    <path
                        d="M447.1 256C447.1 273.7 433.7 288 416 288H109.3l105.4 105.4c12.5 12.5 12.5 32.75 0 45.25C208.4 444.9 200.2 448 192 448s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L109.3 224H416C433.7 224 447.1 238.3 447.1 256z" />
                </svg>
            </a>
            <h1 class="title-list mb-5">Lista de vagas</h1>
        </div>

        @if (session('created'))
            <div data-dismiss="alert" class="alert alert-success" role="alert">
                {{ session('created') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('destroyed'))
            <div data-dismiss="alert" class="alert alert-danger" role="alert">
                {{ session('destroyed') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <main class="main-content">

            <div class="actions">
                <a class="btn btn-success" href="{{ route('vacancy.create') }}" role="button">
                    Adicionar Nova Vaga
                </a>

                <div class="d-flex">

                    <form action="{{ $vacancies->path() }}" class="form-inline" method="GET">
                        <div class="form-group">
                            <input type="hidden" value={{$vacancies->perPage()}} name="per_page">
                            @foreach ($filter_vacancies_type as $type)
                            <input type="hidden" value={{$type}} name="vacancies_type[]">
                            @endforeach
                            <label for="list_order">Ordenar por</label>
                            <select id="list_order" class="form-control ml-3 mr-4" name="order" onchange="this.form.submit()">
                                <option @if ($order == 1) selected @endif value="1">Recentemente criado
                                </option>
                                <option @if ($order == 2) selected @endif value="2">Mais antigos</option>
                                <option @if ($order == 3) selected @endif value="3">Atualizados recentemente
                                </option>
                                <option @if ($order == 4) selected @endif value="4">Mais desatualizado
                                </option>
                            </select>
                        </div>
                    </form>

                    <form action="{{ route('vacancy.index') }}" class="form-inline" method="GET">
                        <div class="form-group">
                            <input type="hidden" value={{$order}} name="order">
                            @foreach ($filter_vacancies_type as $type)
                            <input type="hidden" value={{$type}} name="vacancies_type[]">
                            @endforeach
                            <label for="list_order">resultados por página</label>
                            <select id="list_order" class="form-control ml-3" name="per_page" onchange="this.form.submit()">
                                <option @if($vacancies->perPage() == 20) selected @endif >20</option>
                                <option @if($vacancies->perPage() == 50) selected @endif >50</option>
                                <option @if($vacancies->perPage() == 100) selected @endif >100</option>
                                <option @if($vacancies->perPage() == 1000) selected @endif >1000</option>
                            </select>
                        </div>
                    </form>

                    <button type="button" class="btn btn-secondary ml-4" data-toggle="modal" data-target="#filterModal">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20px" width="20px"
                            fill="#FFFFFF">
                            <path
                                d="M3.853 54.87C10.47 40.9 24.54 32 40 32H472C487.5 32 501.5 40.9 508.1 54.87C514.8 68.84 512.7 85.37 502.1 97.33L320 320.9V448C320 460.1 313.2 471.2 302.3 476.6C291.5 482 278.5 480.9 268.8 473.6L204.8 425.6C196.7 419.6 192 410.1 192 400V320.9L9.042 97.33C-.745 85.37-2.765 68.84 3.854 54.87L3.853 54.87z" />
                        </svg>
                    </button>
                </div>
            </div>

            <form method="GET" action="{{route('vacancy.index')}}">
                <input type="hidden" value={{$vacancies->perPage()}} name="per_page">
                <input type="hidden" value={{$order}} name="order">
                <div class="modal fade" id="filterModal" role="dialog" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="mb-4">
                                <h6>Tipo de contratação</h6>
                                <div class="d-flex justify-content-around mt-3">
                                    @foreach ($vacancies_type as $type)
                                    <div class="form-check">
                                        <input class="form-check-input" @if(in_array($type->id, $filter_vacancies_type)) checked @endif type="checkbox" name="vacancies_type[]" value="{{$type->id}}" id="check{{$type->id}}">
                                        <label class="form-check-label" for="check{{$type->id}}">
                                          {{$type->type}}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <button class="btn btn-secondary" data-dismiss="modal">
                                    Fechar
                                </button>

                                <button class="btn btn-success" type="submit">
                                    Salvar filtros
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <ul class="list">
                @foreach ($vacancies as $vacancy)
                    <li class="item">
                        <a href="{{ route('vacancy.show', ['vacancy' => $vacancy->id]) }}">
                            <div class="item-header">
                                <h3>{{ $vacancy->title }}</h3>
                                <h6>{{ date_format($vacancy->created_at, 'd M') }}</h6>
                            </div>
                            <p>
                                {{ strlen($vacancy->description) > 500 ? substr($vacancy->description, 0, 495) . '...' : $vacancy->description }}
                            </p>
                            <h6>{{ $vacancy->type }}</h6>
                        </a>
                    </li>
                @endforeach
            </ul>

            {{ $vacancies->withQueryString()->links() }}
        </main>
    </div>

@endsection
