@extends('layouts.template')

@section('title')
    @if (empty($vacancy))
        Adicionando nova vaga
    @else
        Atualizando vaga
    @endif
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/vacancy/action.css') }}">
@endsection

@section('content')
    <div class="content">
        <a class="button-back" href="@if(empty($vacancy)) {{ route('vacancy.index') }} @else {{ route('vacancy.show', ['vacancy' => $vacancy->id]) }} @endif">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="30px" widht="30px" fill="#000000">
                <path
                    d="M447.1 256C447.1 273.7 433.7 288 416 288H109.3l105.4 105.4c12.5 12.5 12.5 32.75 0 45.25C208.4 444.9 200.2 448 192 448s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L109.3 224H416C433.7 224 447.1 238.3 447.1 256z" />
            </svg>
        </a>
        <h2 class="title">
            @if (empty($vacancy))
                Adicionando nova vaga
            @else
                Atualizando vaga
            @endif
        </h2>

        <form method="POST"
            action="@if (!empty($vacancy)) {{ route('vacancy.update', ['vacancy' => $vacancy->id ]) }} @else {{ route('vacancy.store') }} @endif"
            class="form-action">
            @csrf
            @if (!empty($vacancy))
                @method("PUT")
            @endif
            <div class="form-group">
                <label for="title">Título da vaga</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title"
                    placeholder="Digite o nome da vaga..."
                    value="@if(!empty($vacancy)){{ $vacancy->title }}@else{{old('title')}} @endif">
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('title') }}
                </div>
            </div>

            <div class="form-group">
                <label for="description">Descrição da vaga</label>
                <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description"
                    placeholder="Descrição da vaga...">
@if (!empty($vacancy))
{{ $vacancy->description }}@else{{ old('description') }}
@endif
</textarea>
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('description') }}
                </div>
            </div>

            <div class="form-group">
                <label for="type">Tipo de contratação</label>
                <select class="form-control" name="vacancies_type_id" id="type">
                    @foreach ($vacancies_type as $type)
                        <option
                            @if (!empty($vacancy)) {{ $vacancy->vacancies_type_id == $type->id ? 'selected' : '' }}@else{{ old('vacancies_type_id') == $type->id ? 'selected' : '' }} @endif
                            value="{{ $type->id }}">
                            {{ $type->type }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <button class="btn btn-success form-control">
                    @if (empty($vacancy))
                        Adicionar
                    @else
                        Atualizar
                    @endif
                </button>
            </div>
        </form>
    </div>
@endsection
