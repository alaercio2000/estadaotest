@extends('layouts.template')

@section('title')
    {{ $vacancy->title }}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/vacancy/show.css') }}">
@endsection

@section('content')
    <div class="content">

        <header>
            <a class="button-back" href="{{ route('vacancy.index') }}">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="30px" widht="30px" fill="#000000">
                    <path
                        d="M447.1 256C447.1 273.7 433.7 288 416 288H109.3l105.4 105.4c12.5 12.5 12.5 32.75 0 45.25C208.4 444.9 200.2 448 192 448s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L109.3 224H416C433.7 224 447.1 238.3 447.1 256z" />
                </svg>
            </a>
            <h2 class="title ml-5">
                {{ $vacancy->title }}
            </h2>

            <div class="list-actions">
                <form action="{{ route('vacancy.paused', ['vacancy' => $vacancy->id]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <button type="submit" class="btn btn-secondary" title="Pausar">
                        @if ($vacancy->paused)
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" height="24px" width="24px"
                                fill="#FFFFFF">
                                <path
                                    d="M361 215C375.3 223.8 384 239.3 384 256C384 272.7 375.3 288.2 361 296.1L73.03 472.1C58.21 482 39.66 482.4 24.52 473.9C9.377 465.4 0 449.4 0 432V80C0 62.64 9.377 46.63 24.52 38.13C39.66 29.64 58.21 29.99 73.03 39.04L361 215z" />
                            </svg>
                        @else
                            <svg xmlns="http://www.w3.org/2000/svg" height="24px" width="24px" viewBox="0 0 320 512"
                                fill="#FFFFFF">
                                <path
                                    d="M272 63.1l-32 0c-26.51 0-48 21.49-48 47.1v288c0 26.51 21.49 48 48 48L272 448c26.51 0 48-21.49 48-48v-288C320 85.49 298.5 63.1 272 63.1zM80 63.1l-32 0c-26.51 0-48 21.49-48 48v288C0 426.5 21.49 448 48 448l32 0c26.51 0 48-21.49 48-48v-288C128 85.49 106.5 63.1 80 63.1z" />
                            </svg>
                        @endif
                    </button>
                </form>

                <a href="{{ route('vacancy.edit', ['vacancy' => $vacancy->id]) }}" class="btn btn-warning" title="Editar">
                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FFFFFF">
                        <path d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z" />
                    </svg>
                </a>

                <form action="{{ route('vacancy.destroy', ['vacancy' => $vacancy->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger" title="Excluir">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px"
                            fill="#FFFFFF">
                            <path d="M0 0h24v24H0z" fill="none" />
                            <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
                        </svg>
                    </button>
                </form>
            </div>
        </header>

        @if (session('updated'))
            <div data-dismiss="alert" class="alert alert-success mt-4" role="alert">
                {{ session('updated') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (!empty(session('paused')))
            <div data-dismiss="alert" class="alert alert-success mt-4" role="alert">
                {{ session('paused') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <h4>Descrição da vaga</h4>
        <p>
            {!! nl2br($vacancy->description) !!}
        </p>

        <span><b>Tipo de contratação:</b> {{ $vacancy->type }}</span>

        <section class="list-candidates">
            @if (session('candidate removed'))
                <div data-dismiss="alert" class="alert alert-danger mt-3" role="alert">
                    {{ session('candidate removed') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if (session('added candidates'))
                <div data-dismiss="alert" class="alert alert-success mt-3" role="alert">
                    {{ session('added candidates') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="d-flex justify-content-between my-4">
                <h3>Candidatos</h3>

                <div class="d-flex">

                    @if ($candidates->all())
                        <button type="button" class="btn btn-danger" data-target="#removeCandidatesModal"
                            data-toggle="modal">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="20px" width="20px"
                                fill="#FFFFFF">
                                <path
                                    d="M135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69zM394.8 466.1C393.2 492.3 372.3 512 346.9 512H101.1C75.75 512 54.77 492.3 53.19 466.1L31.1 128H416L394.8 466.1z" />
                            </svg>
                        </button>
                    @endif

                    @if (!$vacancy->paused)
                        <button type="button" class="btn btn-success ml-4" data-target="#candidatesModal"
                            data-toggle="modal">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="20px" width="20px"
                                fill="#FFFFFF">
                                <path
                                    d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                            </svg>
                        </button>
                    @endif
                </div>
            </div>

            @if ($candidates->all())
                <div class="row">
                    @foreach ($candidates as $candidate)
                        <div class="col-md-6 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <a class="link-title"
                                            href="{{ route('candidate.show', ['candidate' => $candidate->candidates_id]) }}">
                                            <h5 class="card-title">
                                                {{ $candidate->name }}
                                            </h5>
                                        </a>
                                        <h6>{{ date_format($candidate->created_at, 'd M') }}</h6>
                                    </div>

                                    <div class="d-flex justify-content-between">
                                        <p class="card-text">
                                            {{ $candidate->email }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <h6 class="ml-3 mt-3">Não há candidatos</h6>
            @endif

        </section>


        <form method="POST" action="{{ route('vacancy.candidates.add', ['vacancy' => $vacancy->id]) }}">
            <div class="modal fade" id="candidatesModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    @csrf
                    <div class="modal-content p-0">
                        <div class="modal-header">
                            <h5 class="modal-title">Adicionando candidatos na vaga</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5>Candidatos</h5>
                            @foreach ($not_candidates as $candidate)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="candidates[]"
                                        value="{{ $candidate->id }}" id="check{{ $candidate->id }}">
                                    <label class="form-check-label" for="check{{ $candidate->id }}">
                                        {{ $candidate->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-success">Adicionar candidatos</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form method="POST" action="{{ route('vacancy.remove.candidates') }}">
            @csrf
            @method('DELETE')
            <div class="modal fade" id="removeCandidatesModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    @csrf
                    <div class="modal-content p-0">
                        <div class="modal-header">
                            <h5 class="modal-title">Removendo candidatos na vaga</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5>Candidatos</h5>
                            @foreach ($candidates as $candidate)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="vacancies_candidates[]"
                                        value="{{ $candidate->id }}" id="check-remove{{ $candidate->id }}">
                                    <label class="form-check-label" for="check-remove{{ $candidate->id }}">
                                        {{ $candidate->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-danger">Remover candidatos</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
