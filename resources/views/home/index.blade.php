@extends('layouts.template')

@section('title', 'Home')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/home/style.css') }}">
@endsection

@section('content')
    <main class="content">

        <div class="area-logout">
            <h5>Olá {{Auth::user()->name}}</h5>
            <a href="{{route('logout')}}">Sair da conta</a>
        </div>

        <div class="content-main">
            <a href="{{ route('vacancy.index') }}">
                <div class="card">
                    <div class="card-body">
                        Área de vagas
                    </div>
                </div>
            </a>

            <a href="{{ route('candidate.index') }}">
                <div class="card">
                    <div class="card-body">
                        Área de candidatos
                    </div>
                </div>
            </a>
        </div>
    </main>
@endsection
