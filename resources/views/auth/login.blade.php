@extends('layouts.template')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/css/auth/index.css')}}">
@endsection

@section('title', 'Login')

@section('content')
    <div class="content">
        <h2>Entrar no sistema</h2>
        <form method="POST" action="{{route('login.authenticate')}}">
            @csrf
            <div class="form-group">
                <label for="email">E-mail</label>
                <input id="email" class="form-control @error('email') is-invalid @enderror" type="email" name="email" value="{{old('email')}}" placeholder="Digite seu e-mail..." />
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('email') }}
                </div>
            </div>

            <div class="form-group">
                <label for="password">Senha</label>
                <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password" value="{{old('password')}}" placeholder="Digite sua senha..." />
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('password') }}
                </div>
            </div>

            <div class="form-group">
                <span>Não tem conta? </span>
                <a href="{{route('register')}}">Criar</a>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success form-control">
                    Entrar
                </button>
            </div>
        </form>
    </div>
@endsection
