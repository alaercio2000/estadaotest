@extends('layouts.template')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/auth/index.css') }}">
@endsection

@section('title', 'Criar conta')

@section('content')
    <div class="content">
        <h2>Criar conta no sistema</h2>
        <form method="POST" action="{{ route('register.create') }}">
            @csrf

            <div class="form-group">
                <label for="name">Nome</label>
                <input id="name" class="form-control @error('name') is-invalid @enderror" type="text" name="name"
                    value="{{ old('name') }}" placeholder="Digite seu e-mail..." />
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('name') }}
                </div>
            </div>

            <div class="form-group">
                <label for="email">E-mail</label>
                <input id="email" class="form-control @error('email') is-invalid @enderror" type="email" name="email"
                    value="{{ old('email') }}" placeholder="Digite seu e-mail..." />
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('email') }}
                </div>
            </div>

            <div class="form-group">
                <label for="password">Senha</label>
                <input id="password" class="form-control @error('password') is-invalid @enderror" type="password"
                    name="password" value="{{ old('password') }}" placeholder="Digite sua senha..." />
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('password') }}
                </div>
            </div>

            <div class="form-group">
                <label for="password_confirmation">Confirme sua senha</label>
                <input id="password_confirmation" class="form-control @error('password') is-invalid @enderror"
                    type="password" name="password_confirmation" value="{{ old('password_confirmation') }}"
                    placeholder="Digite sua senha..." />
                <div class="invalid-feedback ml-2">
                    {{ $errors->first('password') }}
                </div>
            </div>

            <div class="form-group">
                <span>Já tem conta? </span>
                <a href="{{ route('login') }}">Entrar</a>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success form-control">
                    Entrar
                </button>
            </div>
        </form>
    </div>
@endsection
