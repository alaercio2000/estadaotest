<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacanciesCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies_candidates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vacancies_id');
            $table->unsignedBigInteger('candidates_id');
            $table->timestamps();

            $table->foreign('vacancies_id')->references('id')->on('vacancies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('candidates_id')->references('id')->on('candidates')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies_candidates');
    }
}
