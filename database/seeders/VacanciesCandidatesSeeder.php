<?php

namespace Database\Seeders;

use App\Models\VacanciesCandidates;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VacanciesCandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 1200; $i++) {
            $vacancies_candidates = new VacanciesCandidates();
            $vacancies_candidates->vacancies_id = random_int(1, 150);
            $vacancies_candidates->candidates_id = random_int(1, 400);
            $vacancies_candidates->save();
        }
    }
}
