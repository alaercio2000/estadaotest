<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VacanciesTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vacancies_type')->insert([
            [
                "type" => "CLT",
            ],
            [
                "type" => "Pessoa Jurídica",
            ],
            [
                "type" => "Freelancer",
            ],
        ]);
    }
}
