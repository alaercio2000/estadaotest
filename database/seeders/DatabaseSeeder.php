<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            VacanciesTypeSeeder::class,
            VacancySeeder::class,
            CandidateSeeder::class,
            VacanciesCandidatesSeeder::class,
            UserSeeder::class,
        ]);
    }
}
