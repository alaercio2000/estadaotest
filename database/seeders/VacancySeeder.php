<?php

namespace Database\Seeders;

use App\Models\Vacancy;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory;

class VacancySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create("pt_BR");

        $vacancies_array = [
            "Agrônomo",
            "Bioquímico",
            "Biotecnólogo",
            "Ecologista",
            "Geofísico",
            "Geólogo",
            "Gestor ambiental",
            "Veterinário",
            "Meteorologista",
            "Oceanógrafo",
            "Zootecnólogo",
            "Analista e desenvolvedor de sistemas",
            "Astrônomo",
            "Cientista da computação",
            "Estatístico",
            "Físico",
            "Gestor da tecnologia da informação",
            "Matemático",
            "Nanotecnólogo",
            "Químico",
            "Advogado",
            "Arqueólogo",
            "Cooperativista",
            "Filósofo",
            "Geógrafo",
            "Historiador",
            "Linguista",
            "Museologista",
            "Pedagogo",
            "Professor",
            "Psicopedagogo",
            "Relações internacionais",
            "Sociólogo",
            "Teólogo",
            "Tradutor e intérprete",
            "Arquivologista",
            "Biblioteconomista",
            "Educomunicador",
            "Jornalista",
            "Produtor audiovisual",
            "Produtor cultural",
            "Produtor editorial",
            "Produtor multimídia",
            "Produtor publicitário",
            "Publicitário",
            "Radialista",
            "Relações públicas",
            "Secretária",
            "Secretária executiva",
            "Agricultor",
            "Construtor civil",
            "Construtor naval",
            "Engenheiro acústico",
            "Engenheiro aeronáutico",
            "Engenheiro agrícola",
            "Engenheiro ambiental e sanitário",
            "Engenheiro biomédico",
            "Engenheiro civil",
            "Engenheiro da computação",
            "Engenheiro de alimentos",
            "Engenheiro de biossistemas",
            "Engenheiro de controle e automação",
            "Engenheiro de energia",
            "Engenheiro de inovação",
            "Engenheiro de materiais",
            "Engenheiro de minas",
            "Engenheiro de pesca",
            "Engenheiro de petróleo",
            "Engenheiro de produção",
            "Engenheiro de segurança do trabalho",
            "Engenheiro de sistemas",
            "Engenheiro de software",
            "Engenheiro de telecomunicações",
            "Engenheiro de transporte e mobilidade",
            "Engenheiro elétrico",
            "Engenheiro eletrônico",
            "Engenheiro físico",
            "Engenheiro florestal",
            "Engenheiro hídrico",
            "Engenheiro mecânico",
            "Engenheiro mecatrônico",
            "Engenheiro naval",
            "Engenheiro químico",
            "Gestor da qualidade",
            "Minerador",
            "Silvicultor",
            "Biomédico",
            "Dentista",
            "Educador físico",
            "Enfermeiro",
            "Esteticista",
            "Farmacêutico",
            "Fisioterapeuta",
            "Fonoaudiólogo",
            "Gerontólogo",
            "Gestor em saúde",
            "Gestor hospitalar",
            "Médico",
            "Musicoterapeuta",
            "Nutricionista",
            "Psicólogo",
            "Radiologista",
            "Terapeuta ocupacional",
        ];

        for ($i = 0; $i < 150; $i++) {
            $vacancy = new Vacancy();
            $vacancy->vacancies_type_id = random_int(1, 3);
            $vacancy->title = $vacancies_array[random_int(0, count($vacancies_array) - 1)];
            $vacancy->description = $faker->realText(500);
            $vacancy->save();
        }
    }
}
