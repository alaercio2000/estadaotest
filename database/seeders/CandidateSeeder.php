<?php

namespace Database\Seeders;

use App\Models\Candidate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory;

class CandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create("pt_BR");

        for ($i = 0; $i < 400; $i++) {
            $candidate = new Candidate();
            $candidate->name = $faker->name();
            $candidate->email = $faker->email();
            $candidate->save();
        }
    }
}
