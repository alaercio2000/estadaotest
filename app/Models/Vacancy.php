<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    use HasFactory;

    protected $table = "vacancies";

    protected $fillable = [
        'vacancies_type_id',
        'title',
        'description',
        'paused',
    ];

    public function vacancies_types()
    {
        return $this->hasMany(VacanciesType::class, "vacancies_type_id", "id");
    }

    public function candidades()
    {
        return $this->hasMany(VacanciesCandidates::class, "vacancies_id", "id");
    }
}
