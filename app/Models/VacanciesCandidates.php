<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacanciesCandidates extends Model
{
    use HasFactory;

    protected $table = "vacancies_candidates";

    protected $fillable = [
        'vacancies_id',
        'candidates_id',
    ];
}
