<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    protected $table = "candidates";

    protected $fillable = [
        "name",
        "email",
    ];

    public function vacancies()
    {
        return $this->hasMany(VacanciesCandidates::class, "candidates_id", "id");
    }
}
