<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacanciesType extends Model
{
    use HasFactory;

    protected $table = "vacancies_type";
}
