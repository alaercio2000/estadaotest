<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\VacanciesCandidates;
use App\Models\VacanciesType;
use App\Models\Vacancy;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    private Vacancy $vacancy;
    private VacanciesType $vacancies_type;
    private VacanciesCandidates $vacanciesCandidates;
    private Candidate $candidate;

    function __construct(Vacancy $vacancy, VacanciesType $vacancies_type, VacanciesCandidates $vacanciesCandidates, Candidate $candidate)
    {
        $this->vacancy = $vacancy;
        $this->vacancies_type = $vacancies_type;
        $this->vacanciesCandidates = $vacanciesCandidates;
        $this->candidate = $candidate;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order = request('order', 1);
        $orderBy = ($order == 1 || $order == 2) ? 'created_at' : 'updated_at';
        $sort = $order == 1 || $order == 3 ? 'DESC' : 'ASC';

        $filter_vacancies_type = request('vacancies_type', [1, 2, 3]);
        $perPage = request('per_page', 20);

        $vacancies = $this->vacancy::select('vacancies.id as id', 'vacancies.title as title', 'vacancies.description as description', 'vacancies.created_at as created_at', 'vacancies_type.type as type')
            ->join('vacancies_type', 'vacancies.vacancies_type_id', '=', 'vacancies_type.id')
            ->orderBy($orderBy, $sort)
            ->whereIn('vacancies.vacancies_type_id', $filter_vacancies_type)
            ->paginate($perPage);

        $vacancies_type = $this->vacancies_type::all();

        return view('vacancy.index', [
            "vacancies" => $vacancies,
            "order" => $order,
            "vacancies_type" => $vacancies_type,
            "filter_vacancies_type" => $filter_vacancies_type,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vacancies_type = $this->vacancies_type::all();

        return view('vacancy.action', [
            "vacancies_type" => $vacancies_type
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->DataValidation($request);

        $data = request(['vacancies_type_id', 'title', 'description']);

        $this->vacancy->create($data);

        return redirect()->route('vacancy.index')
            ->with('created', 'Vaga criada com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vacancy = $this->vacancy::select('vacancies.id as id', 'vacancies.paused as paused', 'vacancies.title as title', 'vacancies.description as description', 'vacancies.paused as paused', 'vacancies.created_at as created_at', 'vacancies_type.type as type')
            ->join('vacancies_type', 'vacancies.vacancies_type_id', '=', 'vacancies_type.id')
            ->where('vacancies.id', $id)
            ->first();

        if ($vacancy) {
            $candidates = $vacancy->candidades()
                ->select('vacancies_candidates.id as id', 'vacancies_candidates.created_at as created_at', 'candidates.name as name', 'candidates.email as email', 'candidates.id as candidates_id')
                ->join('candidates', 'vacancies_candidates.candidates_id', '=', 'candidates.id')
                ->get();

            if ($candidates->all()) {
                $ids = [];

                foreach ($candidates as $candidate) {
                    array_push($ids, $candidate->candidates_id);
                }

                $not_candidates = $this->candidate::select('id', 'name', 'email')
                    ->whereNotIn('id', $ids)
                    ->get();
            } else {
                $not_candidates = $this->candidate::all();
            }

            return view('vacancy.show', [
                "vacancy" => $vacancy,
                "candidates" => $candidates,
                "not_candidates" => $not_candidates,
            ]);
        }

        return view('error404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vacancy = $this->vacancy::find($id);
        $vacancies_type = $this->vacancies_type::all();

        if ($vacancy) {
            return view('vacancy.action', [
                "vacancy" => $vacancy,
                "vacancies_type" => $vacancies_type,
            ]);
        }

        return view('error404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->DataValidation($request);

        $data = request(['vacancies_type_id', 'title', 'description']);

        $vacancy = $this->vacancy::find($id);

        $vacancy->update($data);

        return redirect()->route('vacancy.show', [
            'vacancy' => $id
        ])->with('updated', 'Vaga atualizada com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vacancy = $this->vacancy::find($id);
        $vacancy->delete();

        return redirect()->route('vacancy.index')
            ->with('destroyed', 'Vaga excluída com sucesso');
    }

    public function alterPaused($id)
    {
        $vacancy = $this->vacancy::find($id);
        $vacancy->paused = !$vacancy->paused;
        $vacancy->save();

        return redirect()->route('vacancy.show', ['vacancy' => $vacancy->id])
            ->with('paused', $vacancy->paused ? "A vaga está pausada" : "A vaga está liberada");
    }

    public function removeCandidates(Request $request)
    {
        $ids = $request->input('vacancies_candidates');

        foreach ($ids as $id) {
            $vacanciesCandidates = $this->vacanciesCandidates::find($id);
            $vacanciesCandidates->delete();
        }

        $text1 = "O candidato foi removido da vaga";
        $text2 = "Candidato removido da vaga";

        if (count($ids) > 1) {
            $text1 = "Os candidatos foram removidos da vaga";
            $text2 = "Candidato removido das vagas";
        }

        return back()->with([
            'candidate removed' => $text1,
            'vaga removed' => $text2,
        ]);
    }

    public function addCandidates(Request $request, $id)
    {
        $candidates_id = $request->input('candidates');

        foreach ($candidates_id as $candidate_id) {
            $this->vacanciesCandidates->create([
                'vacancies_id' => $id,
                'candidates_id' => $candidate_id,
            ]);
        }

        return back()->with('added candidates', count($candidates_id) > 1 ? 'Os candidatos foram adicionados na vaga' : 'O candidato foi adicionado na vaga');
    }

    private function DataValidation($request)
    {
        return $this->validate($request, [
            'vacancies_type_id' => 'required|numeric|min:1|max:3',
            'title' => 'required|string|min:4|max:150',
            'description' => 'required|string|min:20',
        ], [
            'required' => 'Esse campo é obrigatório',
            'max' => 'O número máximo de caracteres é :max',
            'min' => 'O número mínimo de caracteres é :min',
        ]);
    }
}
