<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    private Auth $auth;
    private User $user;

    function __construct(Auth $auth, User $user)
    {
        $this->middleware('guest');
        $this->auth = $auth;
        $this->user = $user;
    }

    public function index(){
        return view('auth.register');
    }

    public function register(Request $request){
        $this->validate($request ,[
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:4|confirmed'
        ],[
            'max'=>'O número máximo de caracteres é :max',
            'unique'=>'E-mail já cadastrado na plataforma',
            'email.email'=>'Digite um email válido',
            'required'=>'Esse campo é obrigatório',
            'password.min'=>'A senha deve ter :min ou mais caracteres',
            'password.confirmed'=>'As senhas não coincidem'
        ]);
        $data = $request->only([
            'name',
            'email',
            'password',
            'password_confirmation'
        ]);

        $user = $this->user::create($data);

        $this->auth::login($user);

        return redirect()->route('home');
    }
}
