<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\VacanciesCandidates;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private Candidate $candidate;
    private VacanciesCandidates $vacanciesCandidates;

    function __construct(Candidate $candidate, VacanciesCandidates $vacanciesCandidates)
    {
        $this->candidate = $candidate;
        $this->vacanciesCandidates = $vacanciesCandidates;
    }

    public function index()
    {
        $order = request('order', 1);
        $orderBy = ($order == 1 || $order == 2) ? 'created_at' : 'updated_at';
        $sort = $order == 1 || $order == 3 ? 'DESC' : 'ASC';

        $perPage = request('per_page', 20);

        $candidates = $this->candidate::select('id', 'name', 'email', 'created_at')
            ->orderBy($orderBy, $sort)
            ->paginate($perPage);

        return view('candidate.index', [
            "candidates" => $candidates,
            "order" => $order,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidate.action');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->DataValidation($request);

        $data = request(['name', 'email']);

        $this->candidate->create($data);

        return redirect()->route('candidate.index')
            ->with('created', 'Candidato criado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidate = $this->candidate::find($id);

        if ($candidate) {

            $vacancies = $candidate->vacancies()
                ->select('vacancies_candidates.id as id', 'vacancies_candidates.created_at as created_at', 'vacancies.title as title', 'vacancies.description as description', 'vacancies.id as vacancy_id')
                ->join('vacancies', 'vacancies_candidates.vacancies_id', '=', 'vacancies.id')
                ->get();

            return view('candidate.show', [
                'candidate' => $candidate,
                'vacancies' => $vacancies,
            ]);
        }

        return view('error404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = $this->candidate::find($id);

        if ($candidate) {
            return view('candidate.action', [
                'candidate' => $candidate,
            ]);
        }

        return view('error404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->DataValidation($request);

        $data = request(['name', 'email']);

        $candidate = $this->candidate::find($id);

        $candidate->update($data);
        $candidate->save();

        return redirect()->route('candidate.show', ['candidate' => $id])
            ->with('updated', 'Candidato atualizado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = $this->candidate::find($id);
        $candidate->delete();

        return redirect()->route('candidate.index')
        ->with('destroyed', 'Candidato excluído com sucesso');
    }

    public function DataValidation($request)
    {
        return $this->validate($request, [
            'name' => 'required|string|min:4|max:50',
            'email' => 'required|email|max:150',
        ], [
            'required' => 'Esse campo é obrigatório',
            'max' => 'O número máximo de caracteres é :max',
            'min' => 'O número mínimo de caracteres é :min',
            'email' => 'Digite um e-mail válido',
        ]);
    }
}
