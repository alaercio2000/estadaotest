<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    private Auth $auth;

    function __construct(Auth $auth)
    {
        $this->middleware('guest')->except('logout');

        $this->auth = $auth;
    }

    public function index (){
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'email'],
            'password' => ['required'],
        ], [
            'required' => 'Esse campo é obrigatório',
            'email' => 'Digite um e-mail válido',
        ]);

        $data = $request->only([
            'email',
            'password',
        ]);

        if ($this->auth::attempt($data)) {

            return redirect()->route('home');
        }

        return redirect()->route('login')->withErrors([
            'email' => 'Usuário não encontrado'
        ])->withInput();
    }

    public function logout()
    {
        $this->auth::logout();
        return redirect()->route('login');
    }
}
