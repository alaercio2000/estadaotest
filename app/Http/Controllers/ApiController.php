<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\Vacancy;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private Candidate $candidate;
    private Vacancy $vacancy;

    function __construct(Candidate $candidate, Vacancy $vacancy)
    {
        $this->candidate = $candidate;
        $this->vacancy = $vacancy;
    }

    public function ListCandidates()
    {
        $perPage = request('per_page', 20);
        $ordination = request('ordination', 'most recent');

        $orderBy = ($ordination == 'most recent' || $ordination == 'older') ? 'created_at' : 'updated_at';
        $sort = $ordination == 'most recent' || $ordination == 'recently updated' ? 'DESC' : 'ASC';

        $candidates = $this->candidate::select('id', 'name', 'email', 'created_at', 'updated_at')
            ->orderBy($orderBy, $sort)
            ->paginate(intval($perPage));

        return response()->json([
            "page" => $candidates->currentPage(),
            "total" => $candidates->total(),
            "last_page" => $candidates->lastPage(),
            "per_page" => $candidates->perPage(),
            "data" => $candidates->all(),
        ]);
    }

    public function ListVacancies()
    {
        $perPage = request('per_page', 20);
        $ordination = request('ordination', 'most recent');

        $orderBy = ($ordination == 'most recent' || $ordination == 'older') ? 'created_at' : 'updated_at';
        $sort = $ordination == 'most recent' || $ordination == 'recently updated' ? 'DESC' : 'ASC';

        $type = request('type', ['CLT', 'Pessoa Jurídica', 'Freelancer']);

        $vacancy = $this->vacancy::select('vacancies.id as id', 'vacancies.title as title', 'vacancies.description as description', 'vacancies.created_at as created_at', 'vacancies_type.type as type')
            ->join('vacancies_type', 'vacancies.vacancies_type_id', '=', 'vacancies_type.id')
            ->orderBy($orderBy, $sort)
            ->whereIn('vacancies_type.type', $type)
            ->paginate(intval($perPage));

        return response()->json([
            "page" => $vacancy->currentPage(),
            "total" => $vacancy->total(),
            "last_page" => $vacancy->lastPage(),
            "per_page" => $vacancy->perPage(),
            "data" => $vacancy->all(),
        ]);
    }
}
